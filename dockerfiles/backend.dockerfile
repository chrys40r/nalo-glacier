# syntax=docker/dockerfile:1
FROM python:3.9-slim

ARG PROJECT
ARG PROJECT_USER

ENV PROJECT_USER=${PROJECT_USER} \
    PROJECT=${PROJECT}

ENV PYTHONUNBUFFERED=1 \
    PATH=$PATH:/home/${PROJECT_USER}/.local/bin

ENV PYTHONPATH=$PATH:/app


RUN set -x \
 && DEBIAN_FRONTEND=noninteractive apt-get update -qq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -qq -o=Dpkg::Use-Pty=0 --no-install-recommends -y build-essential curl libpcre3-dev wait-for-it \
 && DEBIAN_FRONTEND=noninteractive apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
 && addgroup --gid 1000 --system ${PROJECT_USER} \
 && adduser --disabled-password --system --uid 1000 --ingroup ${PROJECT_USER} ${PROJECT_USER} \
 && mkdir -p /var/log/uwsgi \
 && chown -R 1000:1000 /var/log/uwsgi

COPY --chown=${PROJECT_USER} src/backend /app
COPY --chown=${PROJECT_USER} dockerfiles/scripts/backend-entrypoint-docker.sh /entrypoint.sh

USER ${PROJECT_USER}

WORKDIR /app

RUN set -x \
 && pip install --no-cache-dir --quiet --upgrade pip setuptools \
 && pip install --quiet --no-cache-dir --use-feature=in-tree-build .

ENV DJANGO_SETTINGS_MODULE=${PROJECT}.settings

ENTRYPOINT ["/entrypoint.sh"]
CMD uwsgi --chdir=${PWD} --vacuum --uid=${PROJECT}_USER --gid=${PROJECT}_USER --module=${PROJECT}.wsgi:application --pidfile=/tmp/${PROJECT}.pid --socket=/tmp/${PROJECT}.sock --chmod-socket=660 --http=:80
