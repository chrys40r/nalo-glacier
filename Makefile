ifneq (,)
	$(error "This Makefile requires GNU Make")*
endif

PROJECT=$(shell cat docker.env | grep "^PROJECT=" | cut -d "=" -f2)

.DEFAULT_GOAL := help

.PHONY: $(shell grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | cut -d":" -f1 | tr "\n" " ")

check-defined-% :
	@:$(call check_defined, $*, target-specific)

__docker_compose_cmd=docker-compose --env-file=docker-build.env

check_defined = $(strip $(foreach 1,$1, $(call __check_defined,$1,$(strip $(value 2)))))

__check_defined = $(if $(value $1),, $(error Undefined $1$(if $2, ($2))$(if $(value @), required by target $@)))

########################################
### backend rules
########################################
backend-clean: ## remove useless files => make backend-clean
	@rm -rf docs/backend/build docs/backend/doctrees reports/  docker-build.env
	@find src/backend -name '.coverage*' -exec rm {} \;
backend-coverage: prepare ## get backend test coverage => make backend-coverage
	$(info Make: coverage)
	@${__docker_compose_cmd} run --rm backend-dev bash -c "set -x && \
	pip install --no-cache-dir --user --use-feature=in-tree-build --ignore-installed --quiet /app/.[coverage] && \
	coverage run --rcfile=/app/setup.cfg manage.py test ${PROJECT} && \
	coverage report --rcfile=/app/setup.cfg && \
	coverage report --rcfile=/app/setup.cfg html && \
	coverage annotate --rcfile=/app/setup.cfg -d .annotated"
backend-create-app: prepare check-defined-app_name ## create django app => make backend-create_app app_name=*app_name*
	$(info Make: create app ${app_name})
	@${__docker_compose_cmd} run --rm backend-dev bash -c "set -x && \
	python manage.py startapp -v 3 ${app_name}"
	touch src/backend/${app_name}/{urls,serializers}.py
backend-database-migration: prepare ## apply database migrations => make backend-database-migration
	@${__docker_compose_cmd} run --rm backend-dev bash -c "set -x && \
	python manage.py makemigrations && \
	python manage.py migrate"
backend-documentation: prepare ## Build documentation => make documentation => make backend-documentation
	$(info Make: backend documentation)
	@${__docker_compose_cmd} run --workdir=/ --rm backend-dev bash -c "set -x && \
	pip install --no-cache-dir --user --use-feature=in-tree-build --ignore-installed --quiet /app/.[documentation] && \
	mkdir -p /docs/sources/src && \
	sphinx-apidoc --follow-links --separate --module-first --ext-autodoc --ext-intersphinx --ext-todo --ext-mathjax --ext-viewcode --output-dir /docs/sources/src/${PROJECT} /app/${PROJECT} && \
	sphinx-apidoc --follow-links --separate --module-first --ext-autodoc --ext-intersphinx --ext-todo --ext-mathjax --ext-viewcode --output-dir /docs/sources/src/flavours /app/flavours && \
	sphinx-apidoc --follow-links --separate --module-first --ext-autodoc --ext-intersphinx --ext-todo --ext-mathjax --ext-viewcode --output-dir /docs/sources/src/health /app/health && \
	sphinx-apidoc --follow-links --separate --module-first --ext-autodoc --ext-intersphinx --ext-todo --ext-mathjax --ext-viewcode --output-dir /docs/sources/src/orders /app/orders && \
	sphinx-build --color -T -j auto -b html -c /docs -d /docs/doctrees /docs /docs/build/html"
backend-flake8: ## run flake8 => make backend-flake8
	$(info Make: flake8)
	@${__docker_compose_cmd} run --workdir=/app --rm backend-dev bash -c "set -x && \
	pip install --no-cache-dir --user --use-feature=in-tree-build --ignore-installed --quiet .[flake8] && \
	mkdir -p /app/reports/junit && \
	flake8 --exit-zero --config=/app/setup.cfg --output-file=/app/reports/flake8.txt /app && \
	flake8_junit /app/reports/flake8.txt /app/reports/junit/flake8.xml && \
	flake8 --config=/app/setup.cfg --format=html --htmldir=/app/reports/flake8 /app || true && \
	flake8 --config=/app/setup.cfg /app"
backend-mypy: ## Run mypy on code => make backend-mypy
	$(info Make: mypy)
	@${__docker_compose_cmd} run --workdir=/app --rm backend-dev bash -c "set -x && \
	pip install --no-cache-dir --user --use-feature=in-tree-build --ignore-installed --quiet .[mypy] && \
	mypy --config-file=/app/setup.cfg --cobertura-xml-report=/app/reports/cobertura/mypy.xml --junit-xml=/app/reports/junit/mypy.xml --html-report=/app/reports/mypy-html/ /app"
backend-pylint: ## Run pylint on code => make backend-pylint
	$(info Make: pylint)
	@${__docker_compose_cmd} run --workdir=/app --rm backend-dev bash -c "set -x && \
	pip install --no-cache-dir --user --use-feature=in-tree-build --ignore-installed --quiet .[pylint] && \
	mkdir -p /app/reports/junit && \
	pylint ./src --rcfile=/app/setup.cfg --exit-zero --score=no --reports=no --suggestion-mode=no --output-format=text --msg-template=\"{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}\" > reports/pylint.txt && \
	pylint --exit-zero --rcfile=/app/setup.cfg --output-format=pylint_junit.JUnitReporter ./src > reports/junit/pylint.xml && \
	pylint --exit-zero --rcfile=/app/setup.cfg --output-format=pylint_report.pylint_report.CustomJsonReporter ./src > reports/pylint.json && \
	pylint_report.py reports/pylint.json --html-file reports/pylint.html && \
	pylint --rcfile=/app/setup.cfg --reports=no --fail-under=9 ./src"
backend-reports: prepare ## Build coverage and pylint reports => make backend-reports
	@make --quiet -S backend-syntax-reports || true
	@make --quiet -S backend-coverage || true
backend-syntax-reports: ## check syntax => make backend-syntax-reports
	$(info Make: Check backend syntax reports)
	@make --quiet -S backend-pylint || true
	@make --quiet -S backend-mypy || true
	@make --quiet -S backend-flake8 || true
backend-syntax: ##  launch backend syntax checker => make backend-syntax
	@make --quiet -S backend-flake8 || true
	@make --quiet -S backend-pylint || true
backend-tests: prepare ## Run tests for api => make backend-tests
	$(info Make: tests)
	@${__docker_compose_cmd} run --rm backend-dev bash -c "pip install --quiet tblib==1.7.0 && python manage.py test --verbosity 2"
backend-uwsgi-config: prepare ## display UWSGI config => make backend-uwsgi-config
	$(info Make: get uwsgi config)
	@${__docker_compose_cmd} run --rm backend-prod bash -c "uwsgi --ini /app/setup.cfg --show-config"
backend-uwsgi-logs: prepare ## display UWSGI config => make backend-uwsgi-logs
	$(info Make: get uwsgi logs)
	@${__docker_compose_cmd} exec backend-prod bash -c "cat /var/log/uwsgi/${PROJECT}.log"
backend-upgradable-packages: prepare ## list outdated package in service => make backend-upgradable-packages
	$(info Make: search upgradable package on backend-dev)
	@${__docker_compose_cmd} run --rm backend-dev bash -c "pip list --outdated --format columns"
########################################
### docker rules
########################################
build: prepare ## build containers in parallel one by one => make build [service_name={service_name}]
	$(info Make: Build service ${service_name})
	@${__docker_compose_cmd} build --compress --force-rm ${service_name}
	@make --quiet -S backend-database-migration
build-parallel: prepare ## build containers in parallel => make build-parallel [service_name={service_name}]
	$(info Make: Build service ${service_name})
	@${__docker_compose_cmd} build --compress --force-rm --quiet --parallel ${service_name}
	@make --quiet -S backend-database-migration
connect: prepare check-defined-service_name ## connect to container => make connect [service_name=$service_name]
	$(info Make: connect ${service_name})
	@${__docker_compose_cmd} exec ${service_name} bash || ${__docker_compose_cmd} run --rm ${service_name} bash
config: prepare ## display compiled docker-compose config => make config
	@${__docker_compose_cmd} config
down: prepare ## Down project containers => make down
	$(info Make: Down)
	@${__docker_compose_cmd} down --remove-orphans
logs: prepare check-defined-service_name ## display logs => make logs [service_name={service_name}]
	$(info Make: Logs ${service_name})
	@${__docker_compose_cmd} logs ${service_name}
logs-f: prepare check-defined-service_name ## display logs with follow mode => make logs-f [service_name={service_name}]
	$(info Make: Follow logs ${service_name})
	@${__docker_compose_cmd} logs -f ${service_name}
prune: down ## remove service on the host and prune volume image and network unused => make prune
	$(info Make: Prune)
	@CONTAINER_LIST=$(shell ${__docker_compose_cmd} ps -a -q)
	@if [ ! -z ${CONTAINER_LIST} ]; then \
		${__docker_compose_cmd} rm --force -v; \
	fi
	@OUTDATED_CONTAINER_LIST=$(shell ${__docker_compose_cmd} ps --filter "label.project=${PROJECT}" --filter "status=created" --filter "status=exited" -q)
	@if [ ! -z ${OUTDATED_CONTAINER_LIST} ]; then \
		docker rm ${OUTDATED_CONTAINER_LIST}; \
	fi
	@OUTDATED_IMAGES=$(shell ${__docker_compose_cmd} images --filter "label.project=${PROJECT}" -f "dangling=true" -q)
	@if [ ! -z ${OUTDATED_IMAGES} ]; then \
		docker rm ${OUTDATED_IMAGES}; \
	fi
	@OUTDATED_VOLUMES=$(shell docker volume ls -q)
	@if [ ! -z ${OUTDATED_VOLUMES} ]; then \
		docker volume rm $(shell echo ${OUTDATED_VOLUMES} | grep "${PROJECT}" | tr '\n' ' '); \
	fi
	@make --quiet -S clean
ps: ## run docker-compose ps => make ps
	@${__docker_compose_cmd} ps --all ${service_name}
rebuild: prepare ## Rebuild project containers => make rebuild [service_name={service_name}]
	$(info Make: Rebuild ${service_name})
	@make --quiet -S stop service_name="${service_name}"
	@${__docker_compose_cmd} rm --stop --force ${service_name}
	@make --quiet -S build service_name="${service_name}"
restart: prepare check-defined-service_name ## Restart project containers => make restart [service_name={service_name}]
	$(info Make: Restart ${service_name})
	@make --quiet -S stop service_name="${service_name}"
	@make --quiet -S start service_name="${service_name}"
start: prepare ## Start project containers => make start [service_name={service_name}]
	$(info Make: Start ${service_name})
	@${__docker_compose_cmd} start ${service_name}
stop: prepare ## Stop project containers => make stop [service_name={service_name}]
	$(info Make: Stop ${service_name})
	@${__docker_compose_cmd} stop ${service_name}
up: prepare ## up services => make up [service_name={service_name}]
	$(info Make: up ${service_name})
	@${__docker_compose_cmd} up ${service_name}
up-detach: prepare ## up services => make up-detach [service_name={service_name}]
	$(info Make: up detach ${service_name})
	@${__docker_compose_cmd} up --detach ${service_name}
########################################
### frontend rules
########################################
frontend-build: frontend-install-dependencies ## build frontend => make frontend-build
	@${__docker_compose_cmd} run --rm frontend-dev bash -c "npm run build"
frontend-clean: ## remove useless files => make frontend-clean
	@rm -rf node_modules
frontend-install-dependencies:  ## install package dependencies => make frontend-install-dependencies
	@${__docker_compose_cmd} run --rm frontend-dev bash -c "npm install --quiet --no-audit --legacy-peer-deps --no-package-lock"
frontend-start: frontend-install-dependencies ## launch frontend => make frontend-start
	@${__docker_compose_cmd} run --rm frontend-dev bash -c "npm run start"
frontend-syntax: ## launch syntax checker frontend => make frontend-syntax
	@${__docker_compose_cmd} run --rm frontend-dev bash -c "npm run eslint"
frontend-tests: ## launch tests frontend => make frontend-tests
	@${__docker_compose_cmd} run --rm frontend-dev bash -c "npm run test"
frontend-upgradable-packages: ## list outdated package in services => make frontend-upgradable-packages
	@${__docker_compose_cmd} run --rm frontend-dev bash -c "npm install --quiet --no-audit --no-package-lock npm-check-updates && ncu"
########################################
### general rules
########################################
clean: ## build all documentation frontend => make clean
	@make --quiet -S backend-clean
documentation: ## build all documentation => make documentation
	@make --quiet -S backend-documentation
hadolint: ## lint dockerfiles => make hadolint
	$(info Make: hadolint)
	@echo "=======> hadolint backend"
	@docker run --rm -i -v ${PWD}/.hadolint.yaml:/bin/hadolint.yaml -e XDG_CONFIG_HOME=/bin hadolint/hadolint < dockerfiles/backend.dockerfile
	@echo "=======> hadolint frontend"
	@docker run --rm -i -v ${PWD}/.hadolint.yaml:/bin/hadolint.yaml -e XDG_CONFIG_HOME=/bin hadolint/hadolint < dockerfiles/frontend.dockerfile
help: ## This help dialog. => make help
	@echo "Hello to the ${PROJECT} Makefile\n"
	@echo "Variables:"
	@echo "\t- \"service_name\" is a docker-compose service name or a list of services separate by space as string ($(shell ${__docker_compose_cmd} ps --services | tr '\n' ' '))"
	@echo "\n"
	@IFS=$$'\n'
	@printf "%-50s %-80s %-60s\n" "target" "help" "usage"
	@printf "%-50s %-80s %-60s\n" "------" "----" "----"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | sed 's/:.*##/##/g' | tr ':' ' ' | tr '=>' '##'| awk 'BEGIN {FS = "##"}; {printf "\033[36m%-50s\033[0m %-80s %-60s\n", $$1, $$2, $$3}'
pre-commit: ## run pre-commit pipe => make pre-commit
	$(info Make: pre-commit)
	@pre-commit install-hooks
	@pre-commit autoupdate --bleeding-edge
	@pre-commit run --all-files --hook-stage manual
prepare: ## prepare env file => make prepare
	$(info Make: prepare env file)
	@sed -i 's;^BUILD_DATE.*;BUILD_DATE=$(shell date +"%m-%d-%Y:%H:%M:%S");g' docker.env # this is due to the impossibility of use bash builtins on docker-compose file
	@cp docker.env docker-build.env
	@echo "CI_REGISTRY_IMAGE=$(shell cat docker.env | grep "PROJECT_USER" | cut -d"=" -f2)" >> docker-build.env # this is due to the impossibility of use bash variable to default of another bash variable on docker-compose file
syntax: backend-syntax frontend-synax ## global syntax checker => make syntax
tests: ## launch test for back and front => make tests
	@make --quiet -S backend-tests || true
	@make --quiet -S frontend-tests || true
upgradable-packages: ## list all package upgradable back and front => make upgradable-packages
	@make --quiet -S backend-upgradable-packages || true
	@make --quiet -S frontend-upgradable-packages || true
