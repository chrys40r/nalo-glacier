import React from "react";
import axios from "axios";

export default class FlavourRefillableList extends React.Component {
    state = {
        flavours: []
    }

    componentDidMount() {
        console.log("url : htt:", process.env.REACT_APP_BACKEND_API + "flavour-refill/");
        axios.get(process.env.REACT_APP_BACKEND_API + "flavour-refill/", {
            headers: {"Access-Control-Allow-Origin": "*"},
        }).then(res => {
            console.log("res ", res);
            console.log("res data ", res.data);
            const flavours = res.data;
            this.setState({flavours});
        }).catch(error => {
            console.log(error);
        })
    }

    render() {
        console.log("state ", this.state.flavours);
        return <ul>
            {this.state.flavours.map(flavour => <li>{flavour.name}</li>)}
        </ul>
    }
}
