import React from 'react';
import {BrowserRouter as Router, Link, Route, Switch} from 'react-router-dom';
import Order from './orders/Order'
import FlavourRefillableList from "./flavours/Refillable";
import ApiHealthCheck from "./ApiAvailable";

export default class Menu extends React.Component {
    render() {
        return <div>
            <ApiHealthCheck/>

            <Router>
                <div className="App">
                    <ul className="App-header">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/order">Order</Link>
                        </li>
                        <li>
                            <Link to="#">Retrieve</Link>
                        </li>
                        <li>
                            <Link to="/refill">Refill</Link>
                        </li>
                    </ul>
                    <Switch>
                        <Route exact path='/' component={Order}></Route>
                        <Route exact path='/order' component={Order}></Route>
                        <Route exact path='/refill' component={FlavourRefillableList}></Route>
                    </Switch>
                </div>
            </Router>
        </div>
    }
}
