from http import HTTPStatus

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response


class HealthViewSet(viewsets.ViewSet):  # pylint: disable=too-few-public-methods
    permission_classes = (AllowAny,)

    def list(self, request) -> tuple[dict[str, str], int]:  # pylint: disable=unused-argument, no-self-use
        data: dict[str, str] = {"status": "up"}
        return Response(data=data, status=HTTPStatus.OK)
