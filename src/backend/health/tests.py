from http import HTTPStatus

from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APIClient


class HealthTestCase(TestCase):
    def setUp(self):  # pylint: disable=invalid-name
        self.client = APIClient()
        self.status_fields = [
            'status',
        ]

    def test_health_endpoint_ok(self):
        url = reverse('health-list')
        response = self.client.get(url)
        assert response.status_code == HTTPStatus.OK, f"Expect {HTTPStatus.OK} OK. got: {response.status_code}"
        for field in self.status_fields:
            assert response.json().get("status", {}) == "up", f"Expected field {field} to exist"
