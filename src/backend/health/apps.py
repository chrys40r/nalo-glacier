from django.apps import AppConfig


class HealthConfig(AppConfig):  # pylint: disable=too-few-public-methods
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'health'
