"""glacier URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from __future__ import annotations

from typing import Annotated

from django.contrib import admin
from django.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from flavours.views import FlavoursReFillViewSet
from flavours.views import FlavoursViewSet
from health.views import HealthViewSet
from orders.views import OrdersViewSet

router: Annotated[DefaultRouter, "API routing"] = DefaultRouter()
router.register(r'flavour', FlavoursViewSet, basename='flavour')
router.register(r'flavour-refill', FlavoursReFillViewSet, basename='flavour-refill')
router.register(r'health', HealthViewSet, basename='health')
router.register(r'order', OrdersViewSet, basename='order')

urlpatterns: Annotated[list[path], "front django routes"] = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
]
