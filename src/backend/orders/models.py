from __future__ import annotations

import typing
import uuid
from datetime import datetime

from django.db import models

from flavours.models import Flavours

if typing.TYPE_CHECKING:
    from django.http import Http404


class OrderManager(models.Manager):
    def get_by_identifier_or_404(
        self, *, identifier: typing.Annotated[str, "identifier of order to retrieve"]
    ) -> typing.Annotated[Orders | Http404, "404 error or order object with this identifier"]:
        return self.model.objects.get_object_or_404(identifier=identifier)


class Orders(models.Model):
    class Meta:
        verbose_name = 'order'
        verbose_name_plural = 'orders'
        constraints = [
            models.UniqueConstraint(
                fields=[
                    'identifier',
                ],
                name='unique identifier',
            )
        ]

    creation_date = models.DateTimeField(default=datetime.now)
    total = models.PositiveIntegerField()
    identifier = models.UUIDField(editable=False)

    def __str__(self):
        return f"{self.identifier} {self.creation_date}"

    def generate_identifier(self) -> uuid.UUID:
        return uuid.uuid4()

    def save(self, *args, **kwargs):
        self.identifier = self.generate_identifier()
        super().save(*args, **kwargs)

    objects: typing.Annotated[OrderManager, "collection of aditionnal request function"] = OrderManager()


class OrderContent(models.Model):
    class Meta:
        verbose_name = 'order_content'
        verbose_name_plural = 'order_contents'

    order = models.ForeignKey(Orders, null=True, on_delete=models.CASCADE, related_name='order')
    quantity = models.PositiveIntegerField()
    flavour = models.ForeignKey(Flavours, null=True, on_delete=models.CASCADE, related_name='flavour')
