from django.contrib import admin

from orders.models import OrderContent
from orders.models import Orders

admin.site.register(Orders)
admin.site.register(OrderContent)
