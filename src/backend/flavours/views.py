from __future__ import annotations

import typing
from http import HTTPStatus

from django.http import Http404
from rest_framework import authentication
from rest_framework import permissions
from rest_framework import viewsets
from rest_framework.decorators import authentication_classes
from rest_framework.decorators import permission_classes
from rest_framework.response import Response

from flavours.models import Flavours
from flavours.serializers import FlavoursNameQuantitySerializer
from flavours.serializers import FlavoursSerializer

if typing.TYPE_CHECKING:
    from django.db.models import QuerySet


class FlavoursViewSet(viewsets.ModelViewSet):
    http_method_names: list[str] = ('get',)
    queryset: QuerySet[Flavours] = Flavours.objects.all()
    serializer_class: type[FlavoursSerializer] = FlavoursSerializer


class FlavoursReFillViewSet(viewsets.ModelViewSet):
    http_method_names: list[str] = ('get', 'patch')
    queryset = Flavours.objects.refillable()
    permission_classes = (permissions.IsAdminUser,)

    def update(self, request, pk=None) -> tuple[dict[str, str | FlavoursNameQuantitySerializer], int]:
        status_code: typing.Annotated[int, "status code to send"]
        data: typing.Annotated[dict[str, str | FlavoursNameQuantitySerializer], "data returned"] = {}
        try:
            flavour: typing.Annotated[
                Flavours | Http404, "404 error or flavour with the name"
            ] = Flavours.objects.get_by_name_or_404(name=pk)
        except Http404:
            data['error'] = f"{pk} slug is not found"
            status_code = HTTPStatus.NOT_FOUND
        else:
            if not flavour.quantity:
                flavour.objects.refill()
                data['message'] = f"{flavour} has been refield"
                data['flavour'] = FlavoursNameQuantitySerializer(flavour).data
                status_code = HTTPStatus.OK
            else:
                data['message'] = f"{flavour} is not empty"
                status_code = HTTPStatus.CONFLICT
        return Response(data=data, status=status_code)
