# Serializers define the API representation.
from rest_framework import serializers

from flavours.models import Flavours


class FlavoursSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model: Flavours = Flavours
        fields: list[str] = ['name', 'price', 'quantity']


class FlavoursNameQuantitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Flavours
        fields = ['name', 'quantity']
