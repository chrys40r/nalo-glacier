from django.apps import AppConfig


class FlavoursConfig(AppConfig):
    default_auto_field: str = 'django.db.models.BigAutoField'
    name: str = 'flavours'
