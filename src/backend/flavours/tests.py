from __future__ import annotations

import random
import string
from http import HTTPStatus
from unittest.mock import patch

from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.test import APIClient

from flavours.models import Flavours


class FlavoursTestCase(TestCase):
    def setUp(self):  # pylint: disable=invalid-name
        self.client: APIClient = APIClient()
        self.flavours_response_expected: list[dict[str, str | int]] = [
            {
                "name": "chocolat-orange",
                "price": 2,
            },
            {"name": "maple-walnut", "price": 2},
            {"name": "mint-chocolat", "price": 2},
            {"name": "strawbery-vanille-chocolat", "price": 2},
            {"name": "white-chocolate-rapberry", "price": 2},
        ]
        self.flavours_names = [flav['name'] for flav in self.flavours_response_expected]
        self.flavours_slug = [flav['name'] for flav in self.flavours_response_expected]
        self.superuser_name = 'nalo'
        self.superuser_password = 'nalo'
        self.flavour_list_url = 'flavour:flavour-list'
        self.flavour_refillable_url = 'flavour:flavour-riffell-get'
        self.flavour_update_url = 'flavour:flavour-riffell-update'

    def test_list_status_code(self):
        response = self.client.get(reverse(self.flavour_list_url))
        self.assertEqual(
            response.status_code, HTTPStatus.OK, msg=f"Expect {HTTPStatus.OK} OK. got: {response.status_code}"
        )

    def test_content_size(self):
        response = self.client.get(reverse(self.flavour_list_url))
        self.assertEqual(len(response.json()), 5, msg=f"Expect 5. got: {len(response.json())}")

    # TODO: find a way to get the same behavior of pytest parametrize
    def test_list_content_name(self):
        response = self.client.get(reverse(self.flavour_list_url))
        response_flavour_name = [flavour['name'] for flavour in response.json()]
        for flavour in response_flavour_name:
            self.assertIn(
                flavour,
                self.flavours_names,
                msg="flavour {} not found in ({})".format(flavour, ','.join(self.flavours_names)),
            )

    # TODO: find a way to get the same behavior of pytest parametrize
    def test_list_content_price(self):
        response = self.client.get(reverse(self.flavour_list_url))
        for flavour in response.json():
            expected = [flav for flav in self.flavours_response_expected if flav['name'] == flavour['name']][0]
            self.assertEqual(
                flavour['price'],
                expected['price'],
                msg="Expected price {} for flavor {}, got: {}".format(
                    flavour['price'], flavour['name'], expected['price']
                ),
            )

    # TODO: find a way to get the same behavior of pytest parametrize
    def test_list_content_name(self):
        response = self.client.get(reverse(self.flavour_list_url))
        for flavour in response.json():
            expected = [flav for flav in self.flavours_response_expected if flav['name'] == flavour['name']][0]
            self.assertEqual(
                flavour['name'],
                expected['name'],
                msg="Expected name {}, got: {}".format(flavour['name'], expected['name']),
            )

    def test_refill_unauthenthicated(self):
        flavour = random.choice(self.flavours_response_expected)['name']
        response = self.client.patch(reverse(self.flavour_update_url, args=[flavour]))
        self.assertEqual(
            response.status_code,
            HTTPStatus.FORBIDDEN,
            msg=f"Expect {HTTPStatus.FORBIDDEN} FORBIDDEN. got: {response.status_code}",
        )

    def test_refill_wrong_name(self):
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        response = self.client.patch(
            reverse(self.flavour_update_url, args=[''.join(random.choice(string.ascii_lowercase) for _ in range(10))])
        )
        self.assertEqual(
            response.status_code,
            HTTPStatus.NOT_FOUND,
            msg=f"Expect {HTTPStatus.NOT_FOUND} NOT FOUND. got: {response.status_code}",
        )
        self.client.logout()

    def test_refill_not_empty(self):
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        flavour = random.choice(self.flavours_response_expected)['name']
        response = self.client.patch(reverse(self.flavour_update_url, args=[flavour]))
        self.assertEqual(
            response.status_code,
            HTTPStatus.CONFLICT,
            msg=f"Expect {HTTPStatus.CONFLICT} CONFLICT. got: {response.status_code}",
        )
        self.assertEqual(response.json(), {'message': f"{flavour['name']} is not empty"})
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        self.client.logout()

    def test_refill_empty(self):
        self.client.login(username=self.superuser_name, password=self.superuser_password)
        with patch.object(Flavours.objects, 'get', return_value=self.flavours_response_expected[0]) as mocked_flavour:
            mocked_flavour.quantity = 0
            response = self.client.patch(reverse(self.flavour_update_url, args=[mocked_flavour.name]))
            self.assertEqual(
                response.status_code, HTTPStatus.OK, msg=f"Expect {HTTPStatus.CONFLICT} OK. got: {response.status_code}"
            )
            self.assertEqual(response.json(), {'message': f"{mocked_flavour.name}  has been refield"})
            self.assertEqual(mocked_flavour.quantity, 40)
        self.client.logout()
