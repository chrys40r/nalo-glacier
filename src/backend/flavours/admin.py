from __future__ import annotations

import typing

from django.contrib import admin

from flavours.models import Flavours

if typing.TYPE_CHECKING:
    from django.db.models import QuerySet


class EmptyFalvourListFilter(admin.SimpleListFilter):
    title: typing.Annotated[str, "filter title"] = 'Quantity available'
    parameter_name: str = 'quantity'

    @property
    def trigger(self) -> typing.Annotated[dict[str, int], "define qunatity trigger for each filter"]:
        return {
            "full": Flavours.max_capacity,
            "3_quarter": (Flavours.max_capacity * 0.75),
            "half": (Flavours.max_capacity * 0.5),
            "quarter": (Flavours.max_capacity * 0.25),
            "empty": 0,
        }

    def lookups(self, request, model_admin) -> typing.Annotated[list[tuple(str)], "available filter lists"]:
        return [('full', '100%'), ('3_quarter', '< 75%'), ('half', '< 50%'), ('quarter', '< 25%'), ('empty', '0%')]

    def queryset(
        self, request, queryset: Queryset
    ) -> typing.Annotated[list[Flavours], "Flavours under the rated quantity"]:
        if self.value():
            queryset = queryset.filter(quantity=self.trigger[self.value().lower()])
        return queryset


class FlavoursAdmin(admin.ModelAdmin):
    list_display: typing.Annotated[tuple[str], ""] = ('name', 'quantity', 'price', 'empty')
    list_filter: typing.Annotated[tuple[str], ""] = ('name', EmptyFalvourListFilter)
    list_order: typing.Annotated[tuple[str], "field for orderin"] = ('name', 'quantity', 'price')

    def empty(self, obj) -> typing.Annotated[bool, "define if quantity is empty"]:
        return bool(obj.quantity)

    empty.boolean = True


admin.site.register(Flavours, FlavoursAdmin)
