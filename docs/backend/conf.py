# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import django
from pathlib import Path
import sphinx_rtd_theme


project_path = Path().resolve().parent.resolve() / "src"
sys.path.insert(0, project_path.as_posix())
django.setup()
# -- Project information -----------------------------------------------------

project = os.environ['PROJECT']
copyright = f"2021, {os.environ['PROJECT_USER']}"
author = os.environ['PROJECT_USER']
needs_sphinx = "4.1.1"


# The full version, including alpha/beta/rc tags
master_doc = "sources/index"

# The full version, including alpha/beta/rc t
# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "recommonmark",
    "sphinx.ext.autodoc",
    "sphinx.ext.autodoc.typehints",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.graphviz",
    "sphinx.ext.intersphinx",
    "sphinx.ext.mathjax",
    "sphinx.ext.napoleon",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx_rtd_theme",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = "en"

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_theme_options = {
    "collapse_navigation": True,
    "display_version": True,
    "includehidden": True,
    "navigation_depth": 2,
    "prev_next_buttons_location": "both ",
    "sticky_navigation": True,
    "style_external_links": True,
    "titles_only": False,
}

html_show_sourcelink = True
# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']


html_sidebars = {
    "**": [
        "navigation.html",
        "modules.html",
        "relations.html",
        "searchbox.html",
        "module.html",
    ],
}

# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {
    "python": (
        "https://docs.python.org/3/",
        (None, "http://data.astropy.org/intersphinx/python3.inv"),
    ),
}
# Render inheritance diagrams in SVG
graphviz_output_format = "svg"

graphviz_dot_args = [
    "-Nfontsize=10",
    "-Nfontname=Helvetica Neue, Helvetica, Arial, sans-serif",
    "-Efontsize=10",
    "-Efontname=Helvetica Neue, Helvetica, Arial, sans-serif",
    "-Gfontsize=10",
    "-Gfontname=Helvetica Neue, Helvetica, Arial, sans-serif",
]


# -- Options for todo extension ----------------------------------------------

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True
todo_emit_warnings = True

# -- Options for autodoc extension ----------------------------------------------
autoclass_content = "both"
autodoc_default_flags = [
    "members",
    "undoc-members",
    "private-members",
    "special-members",
    "inherited-members",
    "show-inheritance",
    "ignore-module-all",
    "exclude-members",
]
autodoc_default_options = {
    "member-order": "groupwise",
    "undoc-members": True,
    "private-members": True,
    "special-members": True,
    "inherited-members": True,
    "show-inheritance": True,
    "ignore-module-all": True,
}
autodoc_docstring_signature = True
autosummary_generate = True
autodoc_inherit_docstrings = True
autodoc_member_order = "groupwise"
autodoc_typehints = "signature"

# -- Options for autosectionlabel extension ----------------------------------------------
autosectionlabel_prefix_document = True
autosectionlabel_maxdepth = 10

# -- Markdown ----------------------------------------------------------------
# http://www.sphinx-doc.org/en/stable/usage/markdown.html

# The suffix(es) of {source_folder} filenames.
# You can specify multiple suffix as a list of string:
source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "markdown",
    ".md": "markdown",
}

napoleon_google_docstring = True
napoleon_use_param = True
napoleon_use_ivar = True

napoleon_numpy_docstring = True
napoleon_include_init_with_doc = True
napoleon_include_private_with_doc = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = True
napoleon_use_rtype = True
